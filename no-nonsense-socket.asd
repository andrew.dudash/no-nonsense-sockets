;;;; no-nonsense-socket.asd

(asdf:defsystem #:no-nonsense-socket
  :description "Describe no-nonsense-socket here"
  :author "Andrew Dudash"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :defsystem-depends-on (#:cffi-grovel)
  :depends-on (#:cffi)
  :components ((:file "package")
	       (:cffi-grovel-file "grovel")
               (:file "no-nonsense-socket")))
