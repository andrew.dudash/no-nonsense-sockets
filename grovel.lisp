(in-package #:no-nonsense-socket)

(include "sys/socket.h")

(ctype sock-length "socklen_t")

(constantenum address-family
    ((:af-inet "AF_INET" "PF_INET")
     :documentation "IPv4 Protocol family")
    ((:af-local "AF_UNIX" "AF_LOCAL" "PF_UNIX" "PF_LOCAL")
     :documentation "File domain sockets")
    ((:af-inet6 "AF_INET6" "PF_INET6")
     :documentation "IPv6 Protocol family")
    ((:af-packet "AF_PACKET" "PF_PACKET")
     :documentation "Raw packet access"
     :optional t))
