;;;; no-nonsense-socket.lisp

(in-package #:no-nonsense-socket)

(define-foreign-library libc
  (:unix (:or "libc.so.6")))

(use-foreign-library libc)

(defcfun ("socket" create-socket) :int
  (namespace :int)
  (style :int)
  (protocol :int))

(defcfun ("close" close-socket) :int
  (filedes :int))

(defcstruct socket-address
  (pa_family :unsigned-short)
  (data :char :count 14))

(defctype socket-length :int)

(defcstruct address-info
  (ai_flags :int)
  (ai_family :int)
  (ai_socktype :int)
  (ai_protocol :int)
  (ai_addrlen socket-length)
  (ai_addr (:pointer (:struct socket-address)))
  (ai_canonname :string)
  (ai_next (:pointer (:struct address-info))))

(defcfun ("getaddrinfo" %get-address-info%) :int
  (node :string)
  (service :string)
  (hints (:pointer (:struct address-info)))
  (res (:pointer (:pointer (:struct address-info)))))

(defcfun ("freeaddrinfo" %free-address-info%) :int
  (res (:pointer (:struct address-info))))

;(foreign-funcall "getaddrinfo" :string
;; int getaddrinfo(const char *node, const char *service,
;;                 const struct addrinfo *hints,
;;                 struct addrinfo **res);

(defun get-address-info (&key name service)
  (with-foreign-objects ((hint '(:pointer (:struct address-info)))
			 (result '(:pointer (:struct address-info)))
			 (current '(:pointer (:struct address-info))))
    (%get-address-info% name service hint current)
    (loop while (not (null-pointer-p current))
       collect (with-foreign-slots ((ai_addr ai_addrlen) current (:struct address-info))
		 ai_addrlen)
       do (with-foreign-slots ((ai_next) current (:struct address-info))
	    (setf current ai_next)))
    (%free-address-info% result)))


(defvar *PF-INET* 2)
(defvar *SOCK-STREAM* 1)


(defmacro with-socket ((name namespace style protocol) &body body)
  `(let ((,name (create-socket ,namespace ,style ,protocol)))
     (handler-case
	 (progn ,@body)
       (t () (close-socket ,name)))))
